import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: nowrap;
  background-color: #FFF;
  margin: 20px 20px;
  justify-content: flex-start;
  align-content: flex-start;
  overflow: hidden;
  max-width: 100%;
`;

export const Content = styled.div`
  display:${props => {
    if(props.tamanho > 3 || (props.tamanho === 1 && props.layout[0].length == 2)) {
      return 'flex';
    }
    return 'grid';
  }};
  flex-wrap: wrap;
  grid-template-areas: ${props => {
    try{
      return props.layout.map(item => "\"image" + item.join(' image') + "\" ");
    } catch (err) {
      return "\"image1 image2 image3\"";
    }
  }};
  ${props => {
    try{
      if (props.tamanho > 3)
        return 'align-content: flex-start;';
    } catch (err) {

    }
  }};
  ${props => {
    try{
      if (props.tamanho === 1 && props.layout[0].length === 2)
        return 'justify-content: center;';
    } catch (err) {

    }
  }};
  width: 100%;
  height: ${props => {
    try {
      if (props.tamanho > 3) return '100%';

      if (props.tamanho === 1 && props.layout[0].length === 2) return '100%';

      if (props.tamanho > 1) {
        return '66.6%';
      }
      return '80%';
    } catch (err) {
      return '66.6%';
    }

  }};
  overflow: hidden;
  overflow-y: scroll;
`;

export const Image = styled.div`
  grid-area: ${props => props.area};
  width: ${props => {
    try{
      if (props.tamanho === 1 && props.layout[0].length === 2) return '50%';

      if (props.tamanho > 3) return '33%';

      return '100%';
    } catch (err) {

    }
  }};
  height: ${props => props.tamanho > 3 ? '33%' : '100%'};;
  background-image: url(${props => props.src});
  background-repeat: no-repeat;
  background-size: cover;

  &:hover {
    opacity: 0.8;
  }
`;
