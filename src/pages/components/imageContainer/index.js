import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ImageActions from '../../../store/actions/images';

import { Container, Content, Image } from './styles';

class ImageContainer extends Component {
  componentDidMounth() {
    console.log(this.props);
  }

  render() {
    return (
      <Container>
        <Content layout={this.props.layout} tamanho={this.props.images.length}>
          {console.log(ImageActions)}
          { this.props.images.map((image, indice) =>
              <Image
                key={image.id}
                layout={this.props.layout}
                tamanho={this.props.images.length}
                src={image.data}
                area={'image'+(indice+1)}
                onClick={() => this.props.deleteImage(image.id)} />)
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  images: state.images.images,
  layout: state.images.layout,
});

const mapDispatchToProps = dispatch => bindActionCreators(ImageActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ImageContainer);
