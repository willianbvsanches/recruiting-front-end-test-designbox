import styled from 'styled-components';

export const Image = styled.img`
  display: flex;
  flex-direction: column;
  background-color: #303030;
  max-width: 100%;
  height: auto;
  grid-area: ${(props) => {props.area}};
`;

export const ImageList = styled.div`
  display: grid;
  flex-direction: column;
  flex-wrap: nowrap;
  max-height: 100%;
`;
