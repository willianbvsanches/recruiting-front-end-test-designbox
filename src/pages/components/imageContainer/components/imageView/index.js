import React from 'react';

import { Image, ImageList } from './styles';

const ImageView = ({ imageList }) => {
  return(
    <ImageList>
      {/* imageList.map(image => <Image key={image.id} src={image.data} />) */}
      <Image key={imageList.id} area={String(imageList.id)} src={imageList.data} />
    </ImageList>
  );
}

export default ImageView;
