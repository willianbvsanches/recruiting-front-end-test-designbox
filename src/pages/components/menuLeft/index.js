import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ImageActions from '../../../store/actions/images';
import Loading from '../Loading';

import { Container, DropArea, StyleArea, SpanAviso } from './styles';

class MenuLeft extends Component {
  componentDidMount() {
    window.addEventListener('mouseup', this.prevent);
    window.addEventListener('dragenter', this.prevent);
    window.addEventListener('dragover', this.prevent);
    window.addEventListener('drop', this.dragFile);

    this.props.images
  }

  prevent = event => {
    event.preventDefault();
    event.stopPropagation();

    return false;
  }

  dragFile = (event) => {
    event.preventDefault();
    event.stopPropagation();

    this.abreEvento(event);
    return false;
  }

  abreEvento =  async (event) => {
    try {
      const fileList = event.dataTransfer || event.target;

      const arrayImages = [];

      for (let i = 0; i < fileList.files.length; i++) {
        await arrayImages.push(fileList.files[i]);
      }

      this.props.addImageRequest(arrayImages);

    } catch (err) {
      console.log(err);
    }
  }

  updateLayout = (e) => {
    try {
      this.props.updateLayout(e.target.value);
    } catch (error) {

    }
  }
  render() {
    return (
      <Container>
        <input
          type="file"
          onChange={this.dragFile}
          ref={fileInput => this.fileInput = fileInput}
          multiple
        />
        <DropArea
          onClick={() => this.fileInput.click()}
          onDrop={() => this.dragFile}
        >
        { this.props.images.loading
          ? <Loading />
          : <span>Aperte ou arraste a imagem aqui</span>
        }
        </DropArea>
        <StyleArea onChange={this.updateLayout} value={JSON.stringify(this.props.images.layout)} />
        <SpanAviso>Clique na imagem para excluí-la</SpanAviso>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  images: state.images,
});

const mapDispatchToProps = dispatch => bindActionCreators(ImageActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MenuLeft);
