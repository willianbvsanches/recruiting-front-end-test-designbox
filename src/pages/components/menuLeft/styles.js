import styled from 'styled-components';

import { Spinner } from '../Loading/styles';

export const Container = styled.div`
  min-width: 300px;
  height: 100%;
  background-color: #808080;

  display: flex;
  flex-direction: column;

  padding: 20px;

  input:first-child {
    display: none;
  }
`;

export const DropArea = styled.div`
  min-width: 100%;
  height: 200px;

  border-radius: 20px;
  border: dashed #FFF 1px;

  background-color: #808080;

  display: flex;
  align-items: center;
  justify-content: center;


  ${Spinner} {
    height: 50px;
  }

  span {
    font-size: 20px;
    color: #FFF;
    text-align: center;
    cursor: pointer;
  }
`;

export const StyleArea = styled.textarea`
  width: 100%;
  height: 200px;
  margin-top: 20px;

  border-radius: 20px;
  border: 0;

  background-color: #FFF;
  text-align: center;
`;

export const SpanAviso = styled.span`
  width: 100%;
  margin-top: 30px;
  font-size: 18px;
  color: #FFF;
  text-align: center;
`;
