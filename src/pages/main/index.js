import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ImageActions from '../../store/actions/images';

import { Container } from './styles';
import MenuLeft from '../components/menuLeft';
import ImageContainer from '../components/imageContainer';

class Main extends Component{
  render() {
    return(
      <Container>
        <MenuLeft />
        <ImageContainer>
          { this.props.images.loading &&
            <div style={{width: 200, height: 50, backgroundColor: 'blue'}}>
              <p>progresso: {this.props.images.progress}</p>
            </div> }
        </ImageContainer>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  images: state.images,
});

const mapDispatchToProps = dispatch => bindActionCreators(ImageActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Main);
