import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import createSagaMiddleware from 'redux-saga';

import reducers from './reducers';
import sagas from './sagas';

const middlewares = [];

const sagaMiddleware = createSagaMiddleware();

middlewares.push(sagaMiddleware);

const createAppropriateStore = process.env.NODE_ENV === 'development' ? console.tron.createStore : createStore;

const persistConfig = {
  key: 'alboomRoot',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createAppropriateStore(persistedReducer, compose(applyMiddleware(...middlewares)));
const persistor = persistStore(store);

sagaMiddleware.run(sagas);

export { store, persistor };
