const initialState = {
  loading: false,
  progress: 0,
  images: [],
  layout: [
    [1,1,1,1,1,1,2,2,2],
    [1,1,1,1,1,1,2,2,2],
    [1,1,1,1,1,1,3,3,3],
    [1,1,1,1,1,1,3,3,3],
  ],
};
const layoutPadrao = (indice) => {
  console.log(indice, 'indice');
  switch (indice) {
    case 1:
      return [
        [1,1,1,1,1,1,1,1,1,1,1],
        [1,1,1,1,1,1,1,1,1,1,1],
        [1,1,1,1,1,1,1,1,1,1,1],
        [1,1,1,1,1,1,1,1,1,1,1],
        [1,1,1,1,1,1,1,1,1,1,1],
        [1,1,1,1,1,1,1,1,1,1,1]
      ];
    case 2: return [[1,2]];
    case 3:
      return [
        [1,1,1,1,1,2,2,2],
        [1,1,1,1,1,2,2,2],
        [1,1,1,1,1,3,3,3],
        [1,1,1,1,1,3,3,3],
      ];
    default: return [[]];
  }
};

export default function images(state = initialState, action) {
  switch (action.type) {
    case 'ADD_IMAGE_LOADING':
      return { ...state, loading: true };
    case 'ADD_IMAGE_SUCCESS':
      return {
        ...state,
        images: [
          ...state.images,
          { id: Math.random(), data: action.payload.image },
        ],
        layout: layoutPadrao(state.images.length + 1),
        loading: false,
        process: 0,
      };
    case 'ADD_IMAGE_PROGRESS':
      return { ...state, progress: action.payload.progress };
    case 'UPDATE_LAYOUT':
      return { ...state, layout: JSON.parse(action.payload.layout) };
    case 'DELETE_IMAGE':
      return {
        ...state,
        images: state.images.filter(item => item.id !== action.payload.id),
        layout: layoutPadrao(state.images.length - 1),
      };
    default:
      return state;
  }
}
