import { eventChannel, END, buffers } from 'redux-saga';

export function createUploadImageChannel(file) {
  return eventChannel(emitter => {
    const reader = new FileReader();

    const onProgress = (e) => {
      if( e.loaded < e.total ) {
        const progress = Math.round((e.loaded / e.total) * 100);
        emitter({progress});
      }
    };
    const onError = (e) => {
      emitter({ err: new Error('Falha no Carregamento')});
      emitter(END);
    };

    const onSuccess = () => {
      const result = reader.result;

      emitter({
        success: true,
        data: result,
      });
      emitter(END);
    };

    reader.onprogress = onProgress;
    reader.onerror = onError;
    reader.onload = onSuccess;

    reader.readAsDataURL(file);

    return () => {
      reader.onprogress = null;
      reader.onerror = null;
      reader.onload = null;
      reader.abort();
    };
  }, buffers.sliding(2));
}
