import { call, put, take } from 'redux-saga/effects';

import { addImageSuccess, addImageProgress, addImageLoading } from '../actions/images';
import { createUploadImageChannel } from './imageUploadChannel';

export function* addImage(action) {
  for (let i = 0; i < action.payload.file.length; i++) {

    yield put(addImageLoading(true));

    const channel = yield call(createUploadImageChannel, action.payload.file[i]);

    while(true) {
      const { progress = 0, err, success, data } = yield take(channel);

      if (success) {
        yield put(addImageSuccess(data));
        break;
      }

      if (err) {
        break;
      }

      yield put(addImageProgress(progress));
    }
  }
}
