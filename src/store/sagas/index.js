import { all, takeLatest } from 'redux-saga/effects';

import { addImage } from './images';

export default function* rootSaga() {
  yield all([
    takeLatest('ADD_IMAGE_REQUEST', addImage),
  ]);
}
