export const addImageRequest = file => ({
  type: 'ADD_IMAGE_REQUEST',
  payload: { file },
});

export const addImageLoading = loading => ({
  type: 'ADD_IMAGE_LOADING',
  payload: { loading },
});

export const addImageSuccess = image => ({
  type: 'ADD_IMAGE_SUCCESS',
  payload: { image },
});

export const addImageProgress = progress => ({
  type: 'ADD_IMAGE_PROGRESS',
  payload: { progress },
});

export const updateLayout = layout => ({
  type: 'UPDATE_LAYOUT',
  payload: { layout },
});

export const deleteImage = id => ({
  type: 'DELETE_IMAGE',
  payload: { id },
});
